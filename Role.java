public enum Role {
    Suporte("suporte"),
    Atirador("atirador"),
    Cacador("caçador"),
    Mago("mago"),
    Assassino("assassino"),
    Lutador("lutador"),
    Tank("tank");
    
    private final String descricao;
    
    Role(String descricao){
        this.descricao = descricao;
    }
    
    public String getDescricao(){
        return descricao;
    }
}
