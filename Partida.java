

import java.util.ArrayList;

public class Partida {

    private final int ID;
    private Time timeAzul;
    private Time timeVermelho;
    private int placarAzul;
    private int placarVermelho;
    private Narrador narrador;
    private ArrayList<Campeao> campeoes;

    private static int counter = 0;
    
    public Partida(){
        this.ID = counter++;
    }

    public Partida(Time timeAzul, Time timeVermelho, ArrayList<Campeao> campeoes) {
        this.ID = counter++;
        this.timeAzul = timeAzul;
        this.timeVermelho = timeVermelho;

        if (campeoes == null || campeoes.size() <= 0) {
            this.campeoes = null;
        } 
        else {
            this.campeoes = new ArrayList<>();
            for (int i = 0; i < campeoes.size() ; ++i) {
                this.campeoes.add(campeoes.get(i));
            }
        }
    }

    public int getID() {
        return ID;
    }

    public Time getTimeAzul() {
        return timeAzul;
    }
    
    public void setTimeAzul(Time time){
        timeAzul = time;
    }

    public Time getTimeVermelho() {
        return timeVermelho;
    }
    
     public void setTimeVermelho(Time time){
        timeVermelho = time;
    }

    public int getPlacarAzul() {
        return placarAzul;
    }
    
    public void setPlacarAzul(int placar) {
        placarAzul = placar;
    }

    public int getPlacarVermelho() {
        return placarVermelho;
    }
    
    public void setPlacarVermelho(int placar) {
        placarVermelho = placar;
    }

    public Narrador getNarrador() {
        return narrador;
    }
    
    public void setNarrador(Narrador narrador) {
        this.narrador = narrador;
    }
    
    public void gerarPlacar() {
    }

    public void addJogador(Campeao campeao) {
    }

    public void validarPartida() {
    }

    public void removerJogador(Campeao campeao) {
    }

}
