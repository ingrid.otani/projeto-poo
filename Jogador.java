public final class Jogador extends Pessoa {
    
    private String invocador;
    private Elo elo;
    
    public Jogador(String nome, String dataNasc, String invocador, Elo elo){
        super(nome, dataNasc);
        this.invocador = invocador;
        this.elo = elo;
    }

    public String getInvocador() {
        return invocador;
    }

    public void setInvocador(String invocador) {
        this.invocador = invocador;
    }

    public Elo getElo() {
        return elo;
    }

    public void setElo(Elo elo) {
        this.elo = elo;
    }
    
    public void Pagamento(int valor, float bonus){
        
    }
    
    
}
