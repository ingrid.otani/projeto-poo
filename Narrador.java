public class Narrador extends Pessoa {
    
    private String canal;
    private String transmissao;

    public Narrador(String nome, String dataNasc) {
        super(nome, dataNasc);
    }
       
    public String getCanal() {
        return canal;
    }

    public void setCanal(String canal) {
        this.canal = canal;
    }

    public String getTransmissao() {
        return transmissao;
    }

    public void setTransmissao(String transmissao) {
        this.transmissao = transmissao;
    }

    public void Pagamento(int tempo, float bonus){
        //tempo da partida + bonus
    }
    
}