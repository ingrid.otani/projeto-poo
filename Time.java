import java.util.ArrayList;

public class Time {
    private final int ID;
    private final String nome;
    private ArrayList integrantes;
    private int pontuacao;
    
    private static int counter = 0;
      
    public Time(String nome){
        this.ID = counter++;
        this.nome = nome;
    }
    
    public Time(String nome, ArrayList<Jogador> integrantes){
        this.ID = counter++;
        this.nome = nome;
        
        if (integrantes == null || integrantes.size() <= 0) {
            this.integrantes = null;
        } 
        else {
            this.integrantes = new ArrayList<>();
            for (int i = 0; i < integrantes.size() ; ++i) {
                this.integrantes.add(integrantes.get(i));
            }
        }
    }
   
    public int getID() {
        return ID;
    }

    public String getNome() {
        return nome;
    }

    public ArrayList getIntegrantes() {
        return integrantes;
    }

    public void setIntegrantes(ArrayList integrante) {
        integrantes.add(integrante);
    }

    public int getPontuacao() {
        return pontuacao;
    }
    
    public void aumentarPontuacao(int pontos){
    }
    
    
    
}
