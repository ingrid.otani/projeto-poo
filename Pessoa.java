import java.time.LocalDate;

public class Pessoa {
    
    private final int ID;
    private final String nome;
    private final LocalDate dataNasc; //transformar string em LocalDate  inputDate = LocalDate.parse(date DateTimeFormat.forPattern("dd/MM/yyyy"));                         
    float saldo;
    int idade;
    
    private static int counter = 0;
    
    public Pessoa(String nome, String dataNasc){
        this.ID = counter++;
        this.nome = nome;
        this.dataNasc = LocalDate.parse(dataNasc);
        saldo = 0;
    }

    public void pagamento (int valor){
        // TODO implementar lógica
    }
    
    public void calcularIdade(LocalDate data){
        //Data do sistema - Data do Nascimento
    }

    public int getID() {
        return ID;
    }

    public String getNome() {
        return nome;
    }

    public float getSaldo() {
        return saldo;
    }

    public int getIdade() {
        return idade;
    }   
    
}
