

public class Campeao{
    private final String nome;
    private final Role role;
    private final Jogador jogador;
    
    public Campeao(String nome, Role role, Jogador jogador){
        this.nome = nome;
        this.role = role;
        this.jogador = jogador;
    }

    public String getNome() {
        return nome;
    }

    public Role getRole() {
        return role;
    }


    public Jogador getJogador() {
        return jogador;
    }    
}
