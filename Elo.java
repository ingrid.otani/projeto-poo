

public enum Elo {
    Ferro("ferro"),
    Bronze("bronze"),
    Prata("prata"),
    Ouro("ouro"),
    Platina("platina"),
    Diamante("diamante"),
    Mestre("mestre"),
    GraoMestre("Grão-Mestre"),
    Desafiante("desafiante");
   
    private final String descricao;
    
    Elo(String descricao){
        this.descricao = descricao;
    }
    
    public String getDescricao(){
        return descricao;
    }
}
